package com.onload.www.onload.controller

import com.onload.www.onload.models.Cliente
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import com.onload.www.onload.repository.ClienteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
@RequestMapping("/cliente")
class ClienteController(@Autowired val repo: ClienteRepository) {

    @GetMapping("/")
    fun findAll(): ResponseEntity<List<Cliente>>{
        val clientes = repo.findAll();
        return ResponseEntity.ok(clientes);
    }
    @GetMapping("/{id}")
    fun findOne(@PathVariable("id") id: String): ResponseEntity<Cliente>{
        val cliente = repo.findById(id).orElse(null);
        return ResponseEntity.ok(cliente);
    }
    @PostMapping("/")
    fun save(@RequestBody request: Cliente): ResponseEntity<Cliente>{
        val  data: LocalDateTime = LocalDateTime.now()
        var myFormatObj = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss")


        val cliente = repo.save(Cliente(
            id = data.format(myFormatObj).toString(),
            data = LocalDate.now().toString(),
            nome = request.nome,
            telefone = request.telefone,
            email = request.email,
            cep = request.cep
        ));
        return ResponseEntity.ok(cliente);
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: String, @RequestBody request: Cliente): ResponseEntity<Cliente>{
        val old = repo.findById(id).orElse(null);
        val clienteUpdate = repo.save(Cliente(
            id = old.id,
            data = old.data,
            nome = request.nome,
            telefone = request.telefone,
            email = request.email,
            cep = request.cep,

        ));
        return ResponseEntity.ok(clienteUpdate);
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: String): ResponseEntity<Cliente>{
        val cliente = repo.findOneById(id);
        repo.delete(cliente);
        return ResponseEntity.ok(cliente);
    }
}
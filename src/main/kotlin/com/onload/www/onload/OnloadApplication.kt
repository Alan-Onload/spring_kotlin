package com.onload.www.onload

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OnloadApplication

fun main(args: Array<String>) {
	runApplication<OnloadApplication>(*args)
}

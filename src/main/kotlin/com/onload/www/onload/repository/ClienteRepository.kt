package com.onload.www.onload.repository

import com.onload.www.onload.models.Cliente
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface ClienteRepository : MongoRepository<Cliente,String>{
    fun findOneById(id: String): Cliente override fun deleteAll()
}
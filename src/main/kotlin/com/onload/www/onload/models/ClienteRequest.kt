package com.onload.www.onload.models

class ClienteRequest(
    val id: String,
    val data:String,
    val nome:String,
    val telefone:String,
    val email:String,
    val cep:String,
)
package com.onload.www.onload.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document



@Document
class Cliente(
    @Id
    val id : String = "",
    val data: String = "",
    val nome: String = "",
    val telefone: String = "",
    val email: String = "",
    val cep: String = "",
)